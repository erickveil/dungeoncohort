﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Darkmoor;
using DungeonCohort.JsonLoading;

namespace DungeonCohort
{
    internal class AzagItemGen
    {
        public static string GenItem()
        {
            var prefix = AzagPrefix.ChosePrefix();
            string item = EldMagicItemGen.physicalForm();
            var suffix = AzagSuffix.ChoseSuffix();

            return prefix.Type + " " + item + " " + suffix.Type + "\n" +
                prefix.Effect + "\n" +
                suffix.Effect + "\n";
        }
    }

    class AzagPrefix
    {
        public string Type;
        public string Effect;

        public static string _caveat = "While holding, wearing, or wielding this item ";

        public static AzagPrefix ChosePrefix()
        {
            var table = new RandomTable<AzagPrefix>();

            table.AddItem(new AzagPrefix
            {
                Type = "Null",
                Effect = "Everything that is touching this item is " +
                "rendered non magical or unable to cast spells for as long " +
                "as it is in contact."
            });

            table.AddItem(new AzagPrefix
            {
                Type = "Glyphed",
                Effect = "When anyone holding, wearing, or wielding this " +
                "item would cast a spell, any Exhaustion spent counts as " +
                "double for teh purposes of pushing the spell level."
            });

            table.AddItem(new AzagPrefix
            {
                Type = "Unseen",
                Effect = "This item, and any living thing it is touching " +
                "of Medium size or small, is rendered invisible."
            });


            table.AddItem(new AzagPrefix
            {
                Type = "Nephrite",
                Effect = "While holding, wearing, or wielding this item," +
                " gain +1 to Ability checks."
            });


            table.AddItem(new AzagPrefix
            {
                Type = "Obsidian",
                Effect = "If you would drop to 0 hp while holding, wearing, " +
                "or wielding this item and there is another living " +
                "creature close to you, make an opposed Constitution check " +
                "against them. On a success you take 1d6 hp from them."
            });


            table.AddItem(new AzagPrefix
            {
                Type = "Azure",
                Effect = "Holding wearing or wielding this item grants you " +
                "immunity to the effects of extreme cold."
            });


            table.AddItem(new AzagPrefix
            {
                Type = "Beryl",
                Effect = "All sound neart this item is negated."
            });


            table.AddItem(new AzagPrefix
            {
                Type = "Amaranthine",
                Effect = "While holding, wearing, or wielding this item any " +
                "dice you roll while casting a spell are modified down 1 step."
            });


            table.AddItem(new AzagPrefix
            {
                Type = "Copper",
                Effect = "While holding, wearing, or wielding this item " +
                "if you would lose hp reduce that loss by 1."
            });


            table.AddItem(new AzagPrefix
            {
                Type = "Bone",
                Effect = "This item glows faintly when undead are near it."
            });


            table.AddItem(new AzagPrefix
            {
                Type = "Bronze",
                Effect = "While holding, wearing, or wielding this item " +
                "you maximum Ability Score increases by 1."
            });


            table.AddItem(new AzagPrefix
            {
                Type = "Silver",
                Effect = _caveat + "you maximum hp increases by 6."
            });


            table.AddItem(new AzagPrefix
            {
                Type = "Golden",
                Effect = _caveat + "you gain the Luck feat."
            });

            var die = Dice.Instance;
            int level = die.Roll(1,4) - 1;
            string spell1 = JsonSpellLoader.GetRandomSpellName(level);
            string spell2 = JsonSpellLoader.GetRandomSpellName(level);
            string spell3 = JsonSpellLoader.GetRandomSpellName(level);

            table.AddItem(new AzagPrefix
            {
                Type = "Crystal",
                Effect = _caveat + " can cast the following 3 spells once " +
                "per day: \n" +
                spell1 + "\n" +
                spell2 + "\n" +
                spell3
            }); 


            table.AddItem(new AzagPrefix
            {
                Type = "Ivory",
                Effect = "This item gently pulls in the direction of the " +
                "nearest source of magic."
            });


            table.AddItem(new AzagPrefix
            {
                Type = "Amber",
                Effect = _caveat + "armor does not affect stealth."
            });


            table.AddItem(new AzagPrefix
            {
                Type = "Luminus",
                Effect = "This item emits a pale glow. Anything illusory " +
                "or magically concealed that is illuminated by this glow " +
                "is revealed for what it really is."
            });


            table.AddItem(new AzagPrefix
            {
                Type = "Crimson",
                Effect = _caveat + "grants you immunity from the effects of " +
                "extreme heat."
            });


            table.AddItem(new AzagPrefix
            {
                Type = "Ochre",
                Effect = "Each time this item comes into contact with non " +
                "magical metal roll 1d4; On a 4 nothing happens, on a 2 or " +
                "3 the metal being touched rusts and is destroyed. On a 1 " +
                "both the metal being touched and this item are destoryed."
            });


            table.AddItem(new AzagPrefix
            {
                Type = "Diamond",
                Effect = "When this object would be destroyed, roll 1d6. " +
                "On a 4-6 this object is not destroyed."
            });


            table.AddItem(new AzagPrefix
            {
                Type = "Living",
                Effect = "As an action you may make a DC 10 Int check to " +
                "make this item reshape itself into any mundane item of " +
                "Medium size and Moderate weight or smaller and lighter."
            });


            table.AddItem(new AzagPrefix
            {
                Type = "Mirrored",
                Effect = "If a living creature touches this item they are " +
                "transported to a small extra-dimensional room within it. " +
                "They can leave at any time by touching the walls of this " +
                "space."
            });


            table.AddItem(new AzagPrefix
            {
                Type = "Opal",
                Effect = "Any creature touching this item is frozen in time. " +
                "They cannot be harmed and do not age but are completely " +
                "immobile and unthinking."
            });

            return table.GetResult();
        }

    }

    class AzagSuffix
    {
        public string Type;
        public string Effect;

        public static string _charges = "Roll 1d10 when finding this item. " +
            "This is the number of charges it has. ";

        public static AzagSuffix ChoseSuffix()
        {
            var table = new RandomTable<AzagSuffix>();

            table.AddItem(new AzagSuffix
            {
                Type = "of Azag",
                Effect = "Once per adventure you may beseech Azag for a boon" +
                " involving Magic or Knowledge. The DM will award you " +
                "anything from these categories that you ask, within reason. " +
                "Afterward roll 1d10, on a 1 Azag's servants perceive you as" +
                "a threat."
            });

            table.AddItem(new AzagSuffix
            {
                Type = "of Reflection",
                Effect = "Whenever a spell would affect you, you may make an" +
                " opposed Int check. On a success the spell affects its " +
                "caster in the way it would have affected you instead. " +
                "On a failure this item is destroyed."
            });


            table.AddItem(new AzagSuffix
            {
                Type = "of Charm",
                Effect = "Grants Advantage to all Charisma checks."
            });


            table.AddItem(new AzagSuffix
            {
                Type = "of Aclarity",
                Effect = "while holding, wearing, or wielding this item, you " +
                "may spend 1d4 hp to move up to your full movement without taking " +
                "an action."
            });


            table.AddItem(new AzagSuffix
            {
                Type = "of Memory",
                Effect = "Roll 1d10 when finding this item. This is the " +
                "number of charges it has. Spend 1 charge to restore one " +
                "level of spent spell slot. Multiple charges can be combined " +
                "for higher level spell slots."

            });


            table.AddItem(new AzagSuffix
            {
                Type = "of Dreams",
                Effect = "When you sleep with this item on your person you " +
                "may ask the DM one quesion about the world. They then present " +
                "the answer in the form of a cryptic dream. After you awake " +
                "roll 1d12, on a 1 this item is destroyed."
            });


            table.AddItem(new AzagSuffix
            {
                Type = "of Negation",
                Effect = _charges + "Whenever magic of any kind would " +
                "affect you, negate its effects and remove a charge from " +
                "this item."
            });


            table.AddItem(new AzagSuffix
            {
                Type = "of Regeneration",
                Effect = "At the end of each session of play, automatically " +
                "restore 1d10 hp."
            });


            table.AddItem(new AzagSuffix
            {
                Type = "of the Past",
                Effect = "When this item comes into contact with another " +
                "inanimate object, succeed a DC 10 Int check and the DM will " +
                "answer any one question about the object."
            });


            table.AddItem(new AzagSuffix
            {
                Type = "of Protection",
                Effect = "Add +1 to AC"
            });


            table.AddItem(new AzagSuffix
            {
                Type = "of Might",
                Effect = AzagPrefix._caveat + "you gain Advantage to any Str " +
                "check."
            });


            table.AddItem(new AzagSuffix
            {
                Type = "of Luck",
                Effect = "Gain the Luck Feat. Roll 1d8 each time you use a " +
                "Luck Point. On a roll of 1, this item is destroyed."
            });


            var die = Dice.Instance;
            int level = die.Roll(1, 6);
            string spell = JsonSpellLoader.GetRandomSpellName(level);

            table.AddItem(new AzagSuffix
            {
                Type = "of Sorcery",
                Effect = AzagPrefix._caveat + "you can cast " + spell + ". " +
                "When you do so, roll a 1d8. On a roll of 1, this item is " +
                "destroyed."
            });


            table.AddItem(new AzagSuffix
            {
                Type = "of the Thief",
                Effect = "Succeed a DC 10 Dex check to gain incredible speed. " +
                "You can perform one action before immediately before " +
                "initiative is rolled, and all opponents are treated as " +
                "surprised for the purposes of this action."
            });


            table.AddItem(new AzagSuffix
            {
                Type = "of Ehterelaness",
                Effect = "At any time you may make a DC 10 Int check. " +
                "On a success, you gain the ability to pass through solid " +
                "matter for 10 minutes. On a failure lose half your hp, " +
                "rounded up."
            });


            table.AddItem(new AzagSuffix
            {
                Type = "of the Netherworld",
                Effect = "You may use an action to touch this item to a " +
                "dead creature. if you do so, make a DC 10 Int check. " +
                "On success you may ask one quesiton and their spirit " +
                "will answer through their body. They will not lie but " +
                "can only tell you what they knew in life."
            });


            table.AddItem(new AzagSuffix
            {
                Type = "of Opening",
                Effect = _charges + "You may spend a charge and use an action " +
                "to unlock any lock or open any doorway that you can see."
            });


            table.AddItem(new AzagSuffix
            {
                Type = "of Imprisonment",
                Effect = "This item is possessed by an otherworldly entity. " +
                "At any time you may ask the spirit a question and have the " +
                "DM roll 1d6: on a 1-2 the spirit knows the answer and will " +
                "tell you, on a 3-4 they know but will tell you the " +
                "opposite, on a 5-6 they will not know." 
            });


            table.AddItem(new AzagSuffix
            {
                Type = "of Spell Eating",
                Effect = "Whenever a creature near you casts a spell, you " +
                "may make an opposed Int check. On success, you gain 1d10 " +
                "hp and the spell has no effect."
            });


            table.AddItem(new AzagSuffix
            {
                Type = "of the Leech",
                Effect = AzagPrefix._caveat + "if you deal damage to a " +
                "living creature with a weapon, regain hp equal to the " +
                "damage dealt."
            });


            table.AddItem(new AzagSuffix
            {
                Type = "of Fortune",
                Effect = "When a magic item is found, you may chose to " +
                "roll for a different magic item and take whichever one " +
                "you chose. You cannot do this again until the next session " +
                "of play."
            });


            table.AddItem(new AzagSuffix
            {
                Type = "of Mastery",
                Effect = "Once at the end of each session, you may roll 1d4, " +
                "on a 1 or 2 pick an Ability you are proficient in and " +
                "gain a +1 (not cumulative). This can only affect one Ability " +
                "score at a time. If you roll a 4 this item is destroyed."
            });


            table.AddItem(new AzagSuffix
            {
                Type = "of Zuthaggwa",
                Effect = "Once per adventure you may beseech Zuthaggwa " +
                "to rescue you from a dire situation. Your DM will remove " +
                "the danger, within reason. Afterward roll 1d10, on a roll " +
                "of 1 Zuthaggwa's servants perceive you as a threat."
            });


            return table.GetResult();

        }

    }
}
