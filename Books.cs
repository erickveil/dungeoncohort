﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Darkmoor;

namespace DungeonCohort
{
    class Books
    {
        public static string getBook()
        {
            var dataSource = DataSourceLoader.Instance;
            var bookSource = dataSource.BookSource;
            string jsonBook = bookSource.GetBookSubject();
            string specialBook = CrawlRooms.SpecialBook();
            string furnishBook = DungeonFurnishings.Book();
            string cursedBook = "Cursed Text: " + CrawlRoomTrick.ChooseBane();
            string studyBook = StudyBooks();

            var table = new RandomTable<string>();


            table.AddItem(jsonBook);
            table.AddItem(specialBook);
            table.AddItem(furnishBook);
            table.AddItem(cursedBook);
            table.AddItem(studyBook);

            return table.GetResult();
        }

        public static string StudyBooks()
        {
            var table = new RandomTable<string>();

            table.AddItem("Criminals & Punishers. Legal advice and combat " +
                "tactics. While you possess this, all your attacks with a " +
                "hand weapon deal one extra point of damage.");
            table.AddItem("Ancient Sorceries. Sorcery treaty. WIL save " +
                "(or 1-in-6 chance modified by INT) and you can learn " +
                "one, and only one, 1st level spell from another game " +
                "(or class). You can use that spell once per day.");
            table.AddItem("The Mysteries of Bastion. Sensational stories: " +
                "roll on the general rumours table of your campaign " +
                "world, or come up with something dramatic.");
            table.AddItem("The Bastion Constitution. Never been read, " +
                "full of spores. Suffer d6 STR damage(or lose 3d6 " +
                "hit points).");
            table.AddItem("Dying Speeches and Bloody Murders. Stories " +
                "of criminals and their last words on the gallows. " +
                "A dedicated reader can find clues to their hidden booties.");
            table.AddItem("Bastion Medical Journal. News on medical " +
                "science. It contains a section of ads you can order " +
                "remedies and other elixirs from, as well as " +
                "discount coupons.");

            return "Study Book: " + table.GetResult();
        }
    }
}
