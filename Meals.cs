﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Darkmoor;

namespace DungeonCohort
{
    class Meals
    {

        public static string Meal()
        {
            return Protien() + " with " + Vegetables() + ", " + Vegetables() +
                ", and " + Sweet() + " over " + Base() +
                " with " + Sauce() + " and flavored with " + 
                Flavor();

        }

        public static string Base()
        {
            var table = new RandomTable<string>();
            table.AddItem("Bread");
            table.AddItem("Rice");
            table.AddItem("Noodles");
            table.AddItem("Quinoa");
            table.AddItem("Millet");
            table.AddItem("Oats");
            table.AddItem("Barley");
            table.AddItem("Potatoes");
            table.AddItem("Pie");
            table.AddItem("Grits");
            table.AddItem("Lettus");
            return table.GetResult();
        }

        public static string Protien()
        {
            var table = new RandomTable<string>();
            table.AddItem("Beef");
            table.AddItem("Chicken");
            table.AddItem("Pork");
            table.AddItem("Bacon");
            table.AddItem("Lamb");
            table.AddItem("Venison");
            table.AddItem("Duck");
            table.AddItem("Mystery Meat");
            table.AddItem("Beans");
            table.AddItem("Turkey");
            table.AddItem("Fish");
            table.AddItem("Horse");
            table.AddItem("Rat");
            return table.GetResult();

        }

        public static string Vegetables()
        {
            var table = new RandomTable<string>();
            table.AddItem("Peas");
            table.AddItem("Carrots");
            table.AddItem("Corn");
            table.AddItem("Kale");
            table.AddItem("Cabbage");
            table.AddItem("Broccoli");
            table.AddItem("Brussles Sprouts");
            table.AddItem("Turnips");
            table.AddItem("Asperagus");
            table.AddItem("Tomatoes");
            table.AddItem("Sweet Peppers");
            return table.GetResult();

        }

        public static string Sweet()
        {
            var table = new RandomTable<string>();
            table.AddItem("Grapes");
            table.AddItem("Cherries");
            table.AddItem("Apples");
            table.AddItem("Citrus Fruit");
            table.AddItem("Strawberries");
            table.AddItem("Blueberries");
            table.AddItem("Raspberries");
            table.AddItem("Blackberries");
            table.AddItem("Honey");
            table.AddItem("Dates");
            return table.GetResult();
        }

        public static string Flavor()
        {
            var table = new RandomTable<string>();
            table.AddItem("Fennel Seed");
            table.AddItem("Onion and Garlic");
            table.AddItem("Rosemarry and Shallots");
            table.AddItem("Chives");
            table.AddItem("Chili Spices");
            table.AddItem("Curry");
            table.AddItem("Salt and Pepper");
            table.AddItem("Mustard");
            table.AddItem("Oregano and Basil");
            table.AddItem("Spicy Hot Peppers");
            table.AddItem("Red pepper flakes");
            table.AddItem("Peppercorns and Mustard Seeds");
            return table.GetResult();

        }

        public static string Sauce()
        {
            var table = new RandomTable<string>();
            table.AddItem(Protien() + " broth");
            table.AddItem(Protien() + " stock");
            table.AddItem(Protien() + " gravy");
            table.AddItem("Vegetable broth");
            table.AddItem("a Wine deglazed pan-sauce");
            table.AddItem("thick Cream sauce");
            table.AddItem("Tomato sauce");
            table.AddItem("a Cheese sauce");
            table.AddItem("Oil and Vinegar");
            table.AddItem("Butter");
            return table.GetResult();
        }
    }
}
