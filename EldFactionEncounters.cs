﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Darkmoor;

namespace DungeonCohort
{
    class EldFactionEncounters
    {
        public enum EldFaction {
            Blacktalon,
            Ironbound,
            DarkElf,
            Devil,
            Thraxian,
            Draugr,
            Demon,
            DungeonFauna,
            Flamelord,
            Goji,
            Githyanki,
            Githzeri,
            Goblin,
            Hobgoblin,
            Illithid,
            Insect,
            Guild,
            Necromancer,
            Outlaw,
            Ranger,
            Cult,
            Gangrel,
            Unseelie,
            Daemon
        }

        public static EldFaction ChoseFaction()
        {
            var die = Dice.Instance;
            int lastFaction = (int)EldFaction.Daemon + 1;
            int roll = die.Roll(1, lastFaction) - 1;
            EldFaction faction = (EldFaction)roll;
            return faction;
        }

        public static string ChoseFactionEncounter(EldFaction faction, int level)
        {
            switch (faction)
            {
                case EldFaction.Blacktalon:
                    break;
                case EldFaction.Cult:
                    break;
                case EldFaction.Daemon:
                    break;
                case EldFaction.DarkElf:
                    break;
                case EldFaction.Demon:
                    break;
                case EldFaction.Devil:
                    break;
                case EldFaction.Draugr:
                    break;
                case EldFaction.DungeonFauna:
                    break;
                case EldFaction.Flamelord:
                    break;
                case EldFaction.Gangrel:
                    break;
                case EldFaction.Githyanki:
                    break;
                case EldFaction.Githzeri:
                    break;
                case EldFaction.Goblin:
                    break;
                case EldFaction.Goji:
                    break;
                case EldFaction.Guild:
                    break;
                case EldFaction.Hobgoblin:
                    break;
                case EldFaction.Illithid:
                    break;
                case EldFaction.Insect:
                    break;
                case EldFaction.Ironbound:
                    break;
                case EldFaction.Necromancer:
                    break;
                case EldFaction.Outlaw:
                    break;
                case EldFaction.Ranger:
                    break;
                case EldFaction.Thraxian:
                    break;
                case EldFaction.Unseelie:
                    break;
                default:
                    break;
            }
            return "";
        }

    }
}
